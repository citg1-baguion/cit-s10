package com.zuitt.discussion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;

@SpringBootApplication
@RestController
public class DiscussionApplication {

	public static void main(String[] args) {
		SpringApplication.run(DiscussionApplication.class, args);
	}

	//	DISCUSSION
	//retrieving all posts
	@GetMapping(value = "/posts")
	public String getPosts(){
		return "All posts retrieved.";
	}

	//	creating a new post
	@PostMapping(value = "/posts")
	public String createPost(){
		return "New post created.";
	}

	//retrieving a single post
	@GetMapping(value = "/posts/{postid}")
	public String getPost(@PathVariable Long postid){
		return "Viewing details of post " + postid;
	}

	//	deleting a post
	@DeleteMapping(value = "posts/{postid}")
	public String deletePost(@PathVariable Long postid){
		return "The post " + postid + " has been deleted.";
	}

	//	update a post
	@PutMapping(value = "/posts/{postid}")
	@ResponseBody
	public Post updatePost(@PathVariable Long postid, @RequestBody Post post){
		return post;
	}

	//	retrieving a post for a particular user
	@GetMapping(value = "/myPosts")
	public String getMyPosts(@RequestHeader(value = "Authorization") String user){
		return "Posts for "+ user +" have been retrieved.";
	}



	//	S10 ACTIVITY
	@PostMapping("/users")
	public String createUser(){
		return "New user created";
	}

	@GetMapping("/users")
	public String getUsers(){
		return "All users retrieved";
	}

	@GetMapping("/users/{userid}")
	public String getUser(@PathVariable Long userid){
		return "User "+ userid +" retrieved";
	}

	@DeleteMapping("/users/{userid}")
	public String deleteUser(@PathVariable Long userid, @RequestHeader(value = "Authorization") String user){
		if (user.equals("")) {
			return "Unauthorized access";
		}
		return "The user " +userid+ " has been deleted";
	}

	@PutMapping("/users/{userid}")
	@ResponseBody
	public User updateUser(@PathVariable Long userid, @RequestBody User user){
		return user;
	}
}

